/* pigend -  RFC 3091 implementation
 *
 * Copyright (c) 2020  Alois Wohlschlager
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define _POSIX_C_SOURCE 200809L

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include <mpfr.h>

#define MAIN_PORT 52015 /* 314159 modulo 65536 */
#define APPROX_PORT 23399 /* 220007 modulo 65536 */
#define QUEUE_LENGTH 1024
#define MAX_DIGITS (1 * 1024 * 1024)
#define BUFFER_SIZE (682 * 6) /* largest multiple of six below 4096 */

int pifile;

void child_died(int signal)
{
    int saved_errno = errno;
    while (waitpid(-1, NULL, WNOHANG) > 0);
    errno = saved_errno;
}

int client_main(int client)
{
    off_t offset = 0;
    while (offset < MAX_DIGITS) {
        ssize_t written = sendfile(client, pifile, &offset, MAX_DIGITS - offset);
        if (written < 0) {
            if (errno != EPIPE) perror("sendfile");
            return 1;
        }
    }
    /* not quite compliant (out of digits) */
    close(client);
    close(pifile);
    return 0;
}

int client_approx(int client)
{
    close(pifile);
    char buffer[BUFFER_SIZE];
    const char *digits = "142857";
    for (size_t i = 0; i < BUFFER_SIZE / 6; i++) {
        strcpy(buffer + i * 6, digits);
    }
    size_t offset = 0;
    for (;;) {
        ssize_t written = write(client, buffer + offset, BUFFER_SIZE / 6 * 6 - offset);
        if (written < 0) {
            if (errno != ECONNRESET) perror("write");
            return 1;
        }
        offset = (offset + written) % 6;
    }
}

int bind_tcp_on_port(int fd, uint16_t port)
{
    int result;
    union {
        struct sockaddr_in6 inet;
        struct sockaddr generic;
    } bind_addr;
    memset(&bind_addr.inet, 0, sizeof(struct sockaddr_in6));
    bind_addr.inet.sin6_family = AF_INET6;
    bind_addr.inet.sin6_addr = in6addr_any;
    bind_addr.inet.sin6_port = htons(port);
    result = bind(fd, &bind_addr.generic, sizeof(struct sockaddr_in6));
    if (result < 0) {
        perror("bind");
        close(fd);
        return -1;
    }
    result = listen(fd, QUEUE_LENGTH);
    if (result < 0) {
        perror("listen");
        close(fd);
        return -1;
    }
    return 0;
}

int main(int argc, char **argv)
{
    int result;

    char *tmpdir = getenv("TMPDIR");
    if (tmpdir == NULL) tmpdir = "/tmp";

    int tmp = open(tmpdir, O_DIRECTORY);
    if (tmp < 0) {
        perror("open");
        return 1;
    }
    int tmp_pigend = mkdirat(tmp, "pigend", 0700);
    if (tmp_pigend < 0) {
        if (errno == EEXIST) {
            tmp_pigend = openat(tmp, "pigend", O_DIRECTORY);
            if (tmp_pigend < 0) {
                perror("openat");
                close(tmp);
                return 1;
            }
        } else {
            perror("mkdirat");
            close(tmp);
            return 1;
        }
    }
    close(tmp);
    pifile = openat(tmp_pigend, "digits", O_CREAT | O_WRONLY | O_TRUNC, 0700);
    if (pifile < 0) {
        perror("openat");
        close(tmp_pigend);
        return 1;
    }
    char *buffer = malloc(MAX_DIGITS + 2);
    mpfr_t pi;
    mpfr_init2(pi, (MAX_DIGITS + 1) * 10 / 3);
    mpfr_const_pi(pi, MPFR_RNDD);
    mpfr_exp_t exponent;
    mpfr_get_str(buffer, &exponent, 10, MAX_DIGITS + 1, pi, MPFR_RNDD);
    mpfr_clear(pi);
    size_t offset = 1;
    while (offset <= MAX_DIGITS) {
        ssize_t written = write(pifile, buffer + offset, MAX_DIGITS - offset + 1);
        if (written < 0) {
            perror("write");
            close(tmp_pigend);
            close(pifile);
            return 1;
        }
        offset += written;
    }
    free(buffer);
    close(pifile);
    pifile = openat(tmp_pigend, "digits", O_RDONLY);
    if (pifile < 0) {
        perror("openat");
        close(tmp_pigend);
        return 1;
    }
    close(tmp_pigend);

    puts("digit generation done");

    struct sigaction sa;
    sa.sa_handler = child_died;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
    result = sigaction(SIGCHLD, &sa, NULL);
    if (result < 0) {
        perror("sigaction");
        return 1;
    }

    int main_server = socket(AF_INET6, SOCK_STREAM, 0);
    if (main_server < 0) {
        perror("socket");
        return 1;
    }
    result = bind_tcp_on_port(main_server, MAIN_PORT);
    if (result < 0) {
        close(pifile);
        return 1;
    }
    int approx_server = socket(AF_INET6, SOCK_STREAM, 0);
    if (approx_server < 0) {
        perror("socket");
        return 1;
    }
    result = bind_tcp_on_port(approx_server, APPROX_PORT);
    if (result < 0) {
        close(main_server);
        close(pifile);
        return 1;
    }

    int poller = epoll_create(2);
    if (poller < 0) {
        perror("epoll_create");
        close(approx_server);
        close(main_server);
        close(pifile);
        return 1;
    }
    struct epoll_event main_event;
    main_event.events = EPOLLIN;
    main_event.data.fd = main_server;
    result = epoll_ctl(poller, EPOLL_CTL_ADD, main_server, &main_event);
    if (result < 0) {
        perror("epoll_ctl");
        close(poller);
        close(approx_server);
        close(main_server);
        close(pifile);
        return 1;
    }
    struct epoll_event approx_event;
    approx_event.events = EPOLLIN;
    approx_event.data.fd = approx_server;
    result = epoll_ctl(poller, EPOLL_CTL_ADD, approx_server, &approx_event);
    if (result < 0) {
        perror("epoll_ctl");
        close(poller);
        close(approx_server);
        close(main_server);
        close(pifile);
        return 1;
    }

    struct epoll_event events[2];
    for (;;) {
        int n = epoll_wait(poller, events, 2, -1);
        if (n < 0 && errno != EINTR) { /* SIGCHLD is expected */
            perror("epoll_wait");
        }
        for (int i = 0; i < n; i++) {
            int server = events[i].data.fd;
            union {
                struct sockaddr_in6 inet;
                struct sockaddr generic;
            } peer_addr;
            socklen_t peer_addr_length = sizeof(struct sockaddr_in6);
            int client = accept(server, &peer_addr.generic, &peer_addr_length);
            if (client < 0) {
                perror("accept");
            } else {
                pid_t child = fork();
                if (child < 0) {
                    perror("fork");
                    close(client);
                } else if (child == 0) {
                    close(server);
                    if (server == main_server) {
                        return client_main(client);
                    } else if (server == approx_server) {
                        return client_approx(client);
                    } else {
                        puts("unexpected socket ready");
                        return 1;
                    }
                } else {
                    close(client);
                }
            }
        }
    }
}
